const Row = function (n) {
    let row = document.createElement('tr');
    let cell = `<td class="cell"></td>`
    row.innerHTML = (new Array(n)).fill(cell).join('');
    return row
}

const Table = function(rows, columns) {
    let table = document.createElement('table');
    table.classList.add('playing-field')
    for (i=0; i<rows; i++) {
        table.append(new Row(columns));
    }
    document.body.append(table);
    return table
}

let tbl = new Table(30,30);

document.body.addEventListener('click',function(e){
    console.log(e.target)
    if (e.target.classList.contains('cell')) e.target.classList.toggle('active');
    else if (!e.target.closest('.playing-field')) tbl.classList.toggle('inverse');
});