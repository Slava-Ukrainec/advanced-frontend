HamburgerException = function(message) {
        this.message = message;
        this.name = 'InvalidSpecification';
}

checkInput = function (input, matchWord) {
    try {
        if (!input) {
            throw new HamburgerException('Please, define hamburger ' + matchWord);
        }
        else if (!(input instanceof Object)) {
            throw new HamburgerException('Invalid ' + matchWord + ' reference');
        }
        else if (!input.name.match(matchWord)) {
            throw new HamburgerException('Invalid ' + matchWord + ' characteristic: ' + input.name)
        }
        else return input
    } catch (err) {
        if (err.name === 'InvalidSpecification') {
            console.log(err.message);
        }
        else throw err;
    }
}

Hamburger = function (size, stuffing) {
    Object.defineProperty(this,'_size', {
        value: checkInput(size,'SIZE'),
        writable: false,
        enumerable: true,
        configurable: false
    });

    Object.defineProperty(this,'_stuffing', {
        value: checkInput(stuffing,'STUFFING'),
        writable: false,
        enumerable: true,
        configurable: false
    });

    this._toppings = [];
}

Hamburger.SIZE_SMALL = {name: 'SIZE_SMALL', price: 50,calories: 20 };
Hamburger.SIZE_LARGE = {name: 'SIZE_LARGE', price: 100,calories: 40};
Hamburger.STUFFING_CHEESE = {name: 'STUFFING_CHEESE', price: 10,calories: 20 };
Hamburger.STUFFING_SALAD = {name: 'STUFFING_SALAD', price: 20,calories: 5 };
Hamburger.STUFFING_POTATO = {name: 'STUFFING_POTATO', price: 15,calories: 10 };
Hamburger.TOPPING_SPICE = {name: 'TOPPING_SPICE', price: 15,calories: 0 };
Hamburger.TOPPING_MAYO = {name: 'TOPPING_MAYO', price: 20,calories: 5 };

//for (key in Hamburger) {
//    for (prop in Hamburger[key]) {
//        Object.defineProperty(Hamburger[key],prop, {
//            writable: false,
//            enumerable: true,
//            configurable: false
//        });
//    }
//}

for (key in Hamburger) {
        Object.freeze(Hamburger[key]);
}

Hamburger.prototype.addTopping = function (topping) {
        try {
            if (this._toppings.some(function(item){return topping == item})) throw new HamburgerException (topping.name + ' is already there');
            else {this._toppings.push(checkInput(topping,'TOPPING'))};
        } catch (err) {
            if (err.name === 'InvalidSpecification') console.log(err.message);
            else throw err;
        }
};

Hamburger.prototype.removeTopping = function (topping) {
        try {
            if (this._toppings.some(function(item){return topping == item})) {
                this._toppings = this._toppings.filter(function(item){return topping !== item})
            }
            else throw new HamburgerException (topping.name + ' was not in order')
        } catch (err) {
            if (err.name === 'InvalidSpecification') console.log(err.message);
            else throw err;
        }
};

Hamburger.prototype.getToppings = function () {return this._toppings.map(function(topping){return topping.name})};
Hamburger.prototype.getSize = function () {return this._size.name};
Hamburger.prototype.getStuffing = function () {return this._stuffing.name};
Hamburger.prototype.calcPrice = function () {
    return this._size.price + this._stuffing.price +
        this._toppings.reduce(function(sum, topping){return sum + topping.price},0)
       };
Hamburger.prototype.calcCalories = function () {
    return this._size.calories + this._stuffing.calories +
        this._toppings.reduce(function(sum, topping){return sum + topping.calories},0)
       };


console.log('//////////////////////////////// TESTING /////////////////////////////////////////');
let hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_SALAD);
console.log('///////INITIAL ORDER//////////');
console.log(hamburger);
console.log(`Toppings in current order are: ${hamburger.getToppings()}`);
console.log('////////RE-WRITING PROTECTION//////////////////');
console.log('///////constructor properties//////////');
Hamburger.SIZE_SMALL.name = 'broken_name';
console.log(Hamburger.SIZE_SMALL);
console.log('///////new object properties//////////');
hamburger._size = Hamburger.SIZE_SMALL;
hamburger._stuffing = Hamburger.STUFFING_POTATO;
console.log(`Stuffing to current hamburger is: ${hamburger.getStuffing()}`);
console.log(`Size of current hamburger is: ${hamburger.getSize()}`);
console.log('///////TOPPING_SPICE ADDED/////////');
hamburger.addTopping(Hamburger.TOPPING_SPICE);
console.log(`Toppings in current order are: ${hamburger.getToppings()}`);
console.log('///////ONE NEW + ONE EXISTING TOPPING ADDED/////////');
hamburger.addTopping(Hamburger.TOPPING_SPICE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
console.log(`Toppings in current order are: ${hamburger.getToppings()}`);
//console.log('///////STUFFING ADDED TO TOPPINGS/////////');
//hamburger.addTopping(Hamburger.STUFFING_POTATO);
console.log('///////TOPPING_SPICE REMOVED TWICE/////////');
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log(`Toppings in current order are: ${hamburger.getToppings()}`);
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log(`getToppings ----> Toppings in current order are: ${hamburger.getToppings()}`);
console.log(`getSize ----> Size of current hamburger is: ${hamburger.getSize()}`);
console.log(`getStuffing ----> Stuffing to current hamburger is: ${hamburger.getStuffing()}`);
console.log(`calcPrice ----> Full price of hamburger is: ${hamburger.calcPrice()}`);
console.log(`calcCalories ----> Hamburgers contains ${hamburger.calcCalories()} calories, think twice`);
console.log('///////////////////////////////////INVALID ORDER//////////////////////////');
let hamburger2 = new Hamburger(Hamburger.STUFFING_CHEESE, Hamburger.STUFFING_SALAD);
let hamburger3 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.SIZE_SMALL);
