function blackJackTable() {
    var dealer = [];
    var player = [];
    var deckArray = [];
    pickRandom = function(deckArray) {// private method that removes and returns a random card from deck
        var index = Math.floor(Math.random()* deckArray.length);
        return deckArray.splice(index,1)[0];
    };
    calcCard = function (card){// private method, that calculates scores for specific card (J,Q,K =10, Ace = 1)
        if (card % 13 === 0 || card % 13 > 10) return 10
        else {return card % 13}
    }

    function newRound() { // resets the deck
        deckArray = (new Array(52)).fill(0).map(function(item,index){return index+1});
//    return deckArray
    };

    newRound.shuffle = function() {
        var tempDeck = [];
        n = deckArray.length// allows to shuffle even not full deck, not mandatory, can be replaced by 52 number as in Black Jack it's always 52 cards
        for (var i = 0; i<n; i++) {
            tempDeck.push(pickRandom(deckArray));
        }
        deckArray = tempDeck;
//        return deckArray;
    }
    newRound.deal = function() {
        var playerValues = [];
        var dealerValues = [];
        for (var i = 0; i<2; i++) {
            player.push(pickRandom(deckArray));
            playerValues.push(calcCard(player[i]));
            dealer.push(pickRandom(deckArray));
            dealerValues.push(calcCard(dealer[i]));
        }
        if ([1,10].every(function(wCard){return playerValues.some(function(card){return card === wCard})})) { // checking player hand for Black Jack combination
                this.stop()
                return this.winner()
            }

        else if ([1,10].every(function(wCard){return dealerValues.some(function(card){return card === wCard})})) { // if dealer also has this combination it's a tie
                this.stop();
                return this.winner()
            }
        return {dealer, player}
    }

    newRound.more = function() {
        player.push(pickRandom(deckArray));
        return {dealer: dealer.map((card)=> calcCard(card)), player: player.map((card)=> calcCard(card))}
    }

    newRound.stop = function() {
        var catchUp = player.length - dealer.length;
        if (catchUp > 0) {
            for (i=0; i<catchUp; i++) dealer.push(pickRandom(deckArray));
        }
        var acesArr = [1,14,27,40];// id of Aces cards
        var playerAces = acesArr.filter(function(ace){return player.some(function(card){return card == ace})}).length;// calculating, how many aces on players hand
        player = player.reduce(function(score, card){return score + calcCard(card)},0);// calculating hand scores (ace = 1)
        for (i=0; i < playerAces; i++) { // for each ace we check if adding +10 scores suits player, if not ace remains 1
            if (player+10<=21)  {player = player+10}
        }

        var dealerAces = acesArr.filter(function(ace){return dealer.some(function(card){return card == ace})}).length;
        dealer = dealer.reduce(function(score, card){return score + calcCard(card)},0);
        for (i=0; i < dealerAces; i++) {
            if (dealer+10<=21)  {dealer = dealer+10}
        }

        return {dealer, player}
    }
    newRound.winner = function() {
        if (player > 21) player = 0;
        if (dealer > 21) dealer = 0;
        if (player === dealer) return 'Damn, that was close, it is a tie!';
        else if (dealer > player) return 'Player lost.';
        else return 'Player won!';
    }
    return newRound;
}

var game = blackJackTable()
game();
game.shuffle();
console.log(game.deal());
console.log(game.more());
console.log(game.stop());
console.log(game.winner());




`Реализовать блекджек
1) Перемешиваем карты, раздаем по две карты игроку и дилеру(по очереди)
2) Игрок может взять еще карту или остановиться
3) После того как игрок остановился, дилер добирает карты таким же образом
4) У кого сумма очков больше(но меньше = 21) - тот и выиграл
5*)Реализовать отдельно сам блекджек(то есть (10,J,Q,K)+А - если выпало, то сразу выиграл`

