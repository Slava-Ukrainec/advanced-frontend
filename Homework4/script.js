class HamburgerException {
    constructor (message) {
        this.message = message;
        this.name = 'InvalidSpecification';
    }
}

const checkInput = (input, matchWord) => {
    try {
        if (!input) {
            throw new HamburgerException(`Please, define hamburger ${matchWord}`);
        }
        else if (!(input instanceof Object)) {
            throw new HamburgerException(`Invalid ${matchWord} reference`);
        }
        else if (!input.name.match(matchWord)) {
            throw new HamburgerException(`Invalid ${matchWord} characteristic: ${input.name}`)
        }
        else return input
    } catch (err) {
        if (err.name === 'InvalidSpecification') {
            console.log(err.message);
        }
        else throw err;
    }
}

class Hamburger {
    constructor (size, stuffing) {

        Object.defineProperty(this,'_size', {
            value: checkInput(size,'SIZE'),
            writable: false,
            enumerable: true,
            configurable: false
        });

        Object.defineProperty(this,'_stuffing', {
            value: checkInput(stuffing,'STUFFING'),
            writable: false,
            enumerable: true,
            configurable: false
        });

        this._toppings = [];
    }

    static get SIZE_SMALL() {return {name: 'SIZE_SMALL', price: 50,calories: 20 }};
    static get SIZE_LARGE() {return {name: 'SIZE_LARGE', price: 100,calories: 40}};
    static get STUFFING_CHEESE() {return {name: 'STUFFING_CHEESE', price: 10,calories: 20}};
    static get STUFFING_SALAD() {return {name: 'STUFFING_SALAD', price: 20,calories: 5}};
    static get STUFFING_POTATO() {return {name: 'STUFFING_POTATO', price: 15,calories: 10}};
    static get TOPPING_SPICE() {return {name: 'TOPPING_SPICE', price: 15,calories: 0}};
    static get TOPPING_MAYO() {return {name: 'TOPPING_MAYO', price: 20,calories: 5}};

    addTopping(item) {
        try {
            if (this._toppings.some((topping)=> topping.name == item.name)) throw new HamburgerException (`${item.name} is already there`);
            else {this._toppings.push(checkInput(item,'TOPPING'))};
        } catch (err) {
            if (err.name === 'InvalidSpecification') console.log(err.message);
            else throw err;
        }
    };

    removeTopping(item) {
        try {
            if (this._toppings.some((topping) => topping.name == item.name)) {
                this._toppings = this._toppings.filter((topping)=> topping.name !== item.name)
            }
            else throw new HamburgerException (`${item.name} was not in order`)
        } catch (err) {
            if (err.name === 'InvalidSpecification') console.log(err.message);
            else throw err;
        }

    };

    get Toppings() {return this._toppings.map((topping)=>topping.name)}// returns array with names of all ordered toppings
    get Size() {return this._size.name};
    get Stuffing() {return this._stuffing.name};

    get Price() {
        return this._size.price + this._stuffing.price +
            this._toppings.reduce((sum, topping) => {return sum + topping.price},0)
    };

    get Calories() {
        return this._size.calories + this._stuffing.calories +
            this._toppings.reduce((sum, topping) => {return sum + topping.calories},0)
    };

}


console.log('//////////////////////////////// TESTING /////////////////////////////////////////')
console.log('///////INITIAL ORDER//////////');
let hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_SALAD);
console.log(hamburger);
console.dir(Hamburger);
console.log('///////TOPPING_SPICE ADDED TWICE/////////');
hamburger.addTopping(Hamburger.TOPPING_SPICE);
console.log(`Toppings in current order are: ${hamburger.Toppings}`);
hamburger.addTopping(Hamburger.TOPPING_SPICE);
console.log('///////TOPPING_SPICE REMOVED TWICE/////////');
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log(`Toppings in current order are: ${hamburger.Toppings}`);
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log(`get Size ----> Size of current hamburger is: ${hamburger.Size}`);
console.log(`get Stuffing ----> Stuffing to current hamburger is: ${hamburger.Stuffing}`);
console.log(`get Price ----> Full price of hamburger is: ${hamburger.Price}`);
console.log(`get Calories ----> Hamburgers contains ${hamburger.Calories} calories, the most dietary hamburger ever`);

console.log('///////////////////////////////////INVALID ORDER//////////////////////////');
let hamburger2 = new Hamburger(Hamburger.STUFFING_CHEESE, Hamburger.STUFFING_SALAD);
let hamburger3 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.SIZE_SMALL);
//hamburger.addTopping(Hamburger.STUFFING_CHEESE);

console.log('////////CLASS PROPERTIES PROTECTION//////////////////');
Hamburger.SIZE_SMALL = {name:'broken_name'};
console.log(`Hamburger.SIZE_SMALL = {name:'broken_name'}`);
console.log(Hamburger.SIZE_SMALL);

