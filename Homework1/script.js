let navbar = document.querySelector('.navigation-menu');

const navMouseItemToggle = (e) => {
    if (e.target.classList.contains('navigation-item') && !e.target.classList.contains('nav-item-active')) {
        document.querySelector('.nav-item-active').classList.remove('nav-item-active');
        e.target.classList.add('nav-item-active');
    }
}
const navKeyboardItemToggle = (e) => {
    let navItems = document.querySelectorAll('.navigation-item');
    let indexActive;
    navItems.forEach((item, i)=> item.classList.contains('nav-item-active') ? indexActive = i : false );// searching for currently active item index within existing navItems nodelist
    if (e.key === 'ArrowRight'){
        navItems[indexActive].classList.remove('nav-item-active');
        navItems.length === indexActive + 1 ? navItems[0].classList.add('nav-item-active') : navItems[indexActive + 1].classList.add('nav-item-active');
    }
    else if (e.key === 'ArrowLeft') {
        navItems[indexActive].classList.remove('nav-item-active');
        indexActive === 0 ? navItems[navItems.length - 1].classList.add('nav-item-active') : navItems[indexActive - 1].classList.add('nav-item-active')
    }
    else return false
};


navbar.addEventListener('mouseover',navMouseItemToggle)
document.addEventListener('keydown',navKeyboardItemToggle)